# -*- coding: utf8 -*-

import sys
from events.models import Event as EventModel, Country, City, Place, Tag
from icalendar  import Calendar, Event, vDDDTypes
from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    def handle( *args, **options):
        ical = sys.stdin.read()
        cal = Calendar.from_string(ical)
        #print ical
        for c in cal.walk():
            if type(c) is Event:
                name = c['SUMMARY']
                start = vDDDTypes.from_ical(str(c['DTSTART']))
                end = vDDDTypes.from_ical(str(c['DTEND']))
                loc = c['LOCATION']
                n1 = loc.find(',')
                country = loc[:n1].strip()
                n2 = loc.find(',',n1+1)
                city = loc[n1+1:n2].strip()
                n1 = n2
                n2 = loc.find(':',n1+1)
                address = loc[n1+1:n2].strip()
                place_desc = loc[n2+1:].strip()
                country_obj = Country.objects.get_or_create(name=country)[0]
                city_obj = City.objects.get_or_create(name=city,country=country_obj)[0]
                place = Place.objects.get_or_create(country=country_obj,
                              city=city_obj,
                              address=address, description=place_desc)[0]
                desc = c['DESCRIPTION']
                desc_text = desc[:desc.find('::TAGS ')]
                tags = desc[desc.find('::TAGS ')+7:-2]
                tags.split(',')
                print tags
                tags_list = []
                for t in tags:
                    tags_list += Tag.objects.get_or_create(name=t)

                if desc.find(" ") == -1:
                    event_type_str = desc
                else:
                    event_type_str = desc[:desc.find(" ")]

                event_type = 5
                if event_type_str == u"Конференция":
                    event_type = 1
                if event_type_str == u"Концерт":
                    event_type = 2
                if event_type_str == u"Выставка":
                    event_type = 3
                if event_type_str == u"Спорт":
                    event_type = 4
                
                event = EventModel(name=name, start_date=start.date(),
                      start_time=start.time(),
                      end_date=end.date(), end_time=end.time(), description=desc_text,
                      place=place, event_type=event_type)
                event.save()
                for t in tags_list:
                    event.tags.add(t)

#        pass
        #p = Place(country=Country(loc[0])  , City(loc[1]), Address[2])
        #e = EventModel(name=c['SUMMARY'], start_date=start.date(), start_time=start.time(),
        #              end_date=end.date(), end_time=end.time(), description=c['DESCRIPTION'],
        #              place=)
        