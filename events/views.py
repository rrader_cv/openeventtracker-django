# -*- coding:utf8 -*-

from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.template import RequestContext
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import login_required
from django.utils import simplejson
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http import HttpResponse
from models import *
from django.db.models import Q
import math
from datetime import date

def events_filter(request, page, filters, event_type):
    pagesize = 10
    s = 0
    try:
        count = Event.objects.filter(*filters).distinct().count()
        s = ((int(page)-1)*pagesize)
        events = Event.objects.filter(*filters).distinct()[s:s+pagesize]
        pagecount = int(math.ceil(float(count)/pagesize))
        # tags = set()
        # for event in events:
        #     tags = tags.union(event.tags.all())
        # print tags
    except ObjectDoesNotExist:
        return render_to_response('events/home.html',
                            context_instance=RequestContext(request))
    nextpage = int(page)+1
    if nextpage>pagecount:
        nextpage = None
    prevpage = int(page)-1
    if prevpage<1:
        prevpage = None
    return render_to_response('events/home.html', 
                            {"events_list" : events,
                             "start_num" : s+1,
                             "page" : int(page),
                             "pages" : pagecount,
                             "nextpage" : nextpage,
                             "prevpage" : prevpage,
                             "event_type" : int(event_type),
                             },
                            context_instance=RequestContext(request))

def events(request, page):
    filters = [Q(start_date__gte=date.today())]
    event_type = 1
    if request.GET.has_key("event-type"):
        try:
            lst = request.GET["event-type"].split(',')
            event_type = lst[0]
            filters.append(reduce(Q.__or__, map(lambda x: Q(event_type=int(x)), lst)))
        except ValueError:
            raise Http404
    if request.GET.has_key("tag"):
        try:
            lst = request.GET["tag"].split(',')
            filters.append(reduce(Q.__or__, map(lambda x: Q(tags=int(x)), lst)))
        except ValueError:
            raise Http404
    if request.GET.has_key("place"):
        try:
            lst = request.GET["place"].split(',')
            filters.append(reduce(Q.__or__, map(lambda x: Q(place=int(x)), lst)))
        except ValueError:
            raise Http404
    if request.GET.has_key("creator"):
        try:
            lst = request.GET["creator"].split(',')
            filters.append(reduce(Q.__or__, map(lambda x: Q(creator=int(x)), lst)))
        except ValueError:
            raise Http404
    return events_filter(request, page, tuple(filters), event_type)

def places_lookup(request):
    results = []
    if request.method == "GET":
        if request.GET.has_key(u'query'):
            value = request.GET[u'query']
            if len(value) > 1:
                model_results = Place.objects.filter(address__icontains=value)
                #FIXME: ограничение по количеству
                results = [ x.address for x in model_results ]
    json = simplejson.dumps(results)
    return HttpResponse(json, mimetype='application/json')

@login_required
def add_object(request, form_class, template="events/add_object.html"):
    if request.method != 'POST':
        return render_to_response(template,
                            {"form" : form_class},
                            context_instance=RequestContext(request))
    form = form_class(request.POST)
    #form.additional_data = {"place_str" : request.POST["id_place_str"]}
    place = None
    if form.is_valid():
        obj = form.save(commit=False)
        if hasattr(form, "save_extra"):
            form.save_extra(obj, request)
        obj = form.save(commit=True)
    else:
        return render_to_response(template,
                            {"form" : form},
                            context_instance=RequestContext(request))
    return render_to_response('events/added_successful.html',
                            {"object" : obj, 'next' : '/view/%s/%s' % (form.Meta.url_str,obj.id), "name" : form.Meta.object_name},
                            context_instance=RequestContext(request))

def view_event(request, num):
    try:
        event = Event.objects.get(id=int(num))
    except Event.DoesNotExist:
        raise Http404
    return render_to_response('events/view_event.html',
                            {"event" : event,
                             "tags" : event.tags.all()},
                            context_instance=RequestContext(request))
