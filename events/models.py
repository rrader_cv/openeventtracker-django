# -*- coding: utf8 -*-

from django.db import models
from django.contrib.auth.models import User

class Tag(models.Model):
    name = models.CharField(max_length=50)
    def __unicode__(self):
        return self.name

# Place models

class PlaceModel(models.Model):
    class Meta:
        abstract = True
    
    # @classmethod
    # def get_instance(cls, **params):
    #     try:
    #         r = cls.objects.get(**params)
    #     except cls.DoesNotExist:
    #         r = cls(**params)
    #         r.save()
    #     return r

class Country(PlaceModel):
    name = models.CharField(max_length=100)
    def __unicode__(self):
        return unicode(self.name)

class City(PlaceModel):
    name = models.CharField(max_length=100)
    country = models.ForeignKey(Country)
    def __unicode__(self):
        return unicode(self.name)

class Place(models.Model):
    description = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    city = models.ForeignKey(City)
    country = models.ForeignKey(Country)
    def __unicode__(self):
        desc = u"%s, %s" % (self.country, self.city)
        address = unicode(", "+self.address) if self.address is not None \
                                    else ''
        description = unicode(self.description) if self.description is not None \
                                    else ''
        return "%s%s:%s" % (desc, address, description)

# Event models

class Event(models.Model):
    id = models.AutoField(primary_key=True) 
    name = models.CharField(max_length=100)
    start_date = models.DateField()
    start_time = models.TimeField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    place = models.ForeignKey(Place, blank=True, null=True)
    event_type = models.IntegerField()
    tags = models.ManyToManyField(Tag)
    creator = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_creator", blank=True, null=True)
    intrested = models.ManyToManyField(User, related_name="%(app_label)s_%(class)s_intrested")
    will_go = models.ManyToManyField(User, related_name="%(app_label)s_%(class)s_will_go")

    class Meta:
        ordering = ('start_date',)

# Concert models
class Musician(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()

class MusicStyle(models.Model):
    name = models.CharField(max_length=100)

class MusicBand(models.Model):
    name = models.CharField(max_length=100)
    musicians = models.ManyToManyField(Musician)
    tags = models.ManyToManyField(Tag)
    music_style = models.ManyToManyField(MusicStyle)

class MusicEvent(Event):
    bands = models.ManyToManyField(MusicBand)


# IT Conference models

class ITTechnology(models.Model):
    name = models.CharField(max_length=100)

class ITEvent(Event):
    technology = models.ManyToManyField(ITTechnology)

# Model for urls
class URLModel(models.Model):
    url = models.CharField(max_length=100)
    begin = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    start_date = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    tags = models.CharField(max_length=100)