from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to, direct_to_template
from OET.events.forms import *

urlpatterns = patterns('events.views',
    # Examples:
    # url(r'^$', 'WillBe.views.home', name='home'),
    url(r'^events/$', 'events', {"page" : 1}, name="events_list"),
    url(r'^events/(?P<page>\d+)$', 'events', name="events_list"),
    url(r'^events/filter/(?P<page>\d+)$', 'events', name="events_list_filter"),
    url(r'^events/filter/$', 'events', {"page" : 1}, name="events_list_filter"),
    url(r'^view/event/(?P<num>\d+)$', 'view_event', name='event_view'),

    url(r'^add/place/$', 'add_object', {"form_class" : AddPlaceForm}, name='place_add'),
    #url(r'^add/event-type/$', 'add_object', {"form_class" : AddEventTypeForm}, name='event_type_add'),
    url(r'^add/tag/$', 'add_object', {"form_class" : AddTagForm}, name='tag_add'),

    url(r'^add/event/$', 'add_object', {"form_class" : AddEventForm, 
                                  "template" : "events/add.html"}, name='event_add'),

    url(r'^add/url/$', 'add_object', {"form_class" : AddURLForm, 
                                  "template" : "events/add.html"}, name='event_add'),

    url(r'^edit/$', redirect_to, {'url': '/not-implemented/', 'permanent': False}, name='event_edit'),
    url(r'^del/$', redirect_to, {'url': '/not-implemented/', 'permanent': False}, name='event_del'),

    url(r'^view/place/(?P<num>\d+)$', redirect_to, {'url': '/not-implemented/', 'permanent': False}, name='place_view'),
    url(r'^view/event-type/(?P<num>\d+)$', redirect_to, {'url': '/not-implemented/', 'permanent': False}, name='event_type_view'),
    url(r'^view/tag/(?P<num>\d+)$', redirect_to, {'url': '/not-implemented/', 'permanent': False}, name='tag_view'),

    url(r'^lookup/places/$', 'places_lookup', {'url': '/not-implemented/', 'permanent': False}, name='places_lookup'),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)