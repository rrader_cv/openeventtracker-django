# -*- coding:utf8 -*-

from django import forms
from django.forms import ModelForm, Textarea
from models import *

class AddEventForm(ModelForm):
    class Meta:
        model = Event
        exclude = ('creator', 'intrested', 'will_go')
        url_str = "event"
        object_name = "Событие"
    
    def save_extra(self, obj, request):
        obj.creator = request.user

class AddPlaceForm(forms.Form):
    country = forms.CharField(max_length=100)
    city = forms.CharField(max_length=100)
    address = forms.CharField(max_length=150, required=False)
    description = forms.CharField(max_length=100, required=False)

    class Meta:
        url_str = "place"
        object_name = "Место"

    def save(self, commit=True):
        if not commit:
            return None
        place = Place()
        place.country = Country.get_instance(name=self.cleaned_data['country'])
        place.city = City.get_instance(name=self.cleaned_data['city'], country=place.country)
        place.address = None
        if (self.cleaned_data['address'] is not None) and (self.cleaned_data['address'] != ''):
            place.address = Address.get_instance(name=self.cleaned_data['address'], city=place.city)
        place.description = self.cleaned_data['description']
        place.save()
        return place

class AddTagForm(ModelForm):
    class Meta:
        model = Tag
        url_str = "tag"
        object_name = "Тег"

class AddURLForm(ModelForm):
    class Meta:
        model = URLModel
        url_str = "url"
        object_name = "URL"