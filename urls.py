from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to
from django.views.generic.simple import direct_to_template

from OET.events.forms import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('OET.views',
    url(r'^$', redirect_to, {'url': '/events/', 'permanent': False}, name='home'),
    #url(r'^oet/', include('OET.events.urls')),

    url(r'^users/login/$', 'login_user', name='login'),
    url(r'^users/login/$', 'login_user', name='login'),
    url(r'^users/logout/$', 'logout_user', name='logout'),
    url(r'^users/registration/$', 'registration', name='registration'),

    url(r'^users/profile/$', 'my_profile', name='user_profile'),
    url(r'^users/profile/(?P<user>\d+)$', 'user_profile', name='user_profile'),

    url(r'^not-implemented/$', direct_to_template, {'template':'not_implemented.html'}, name='not_implemented'),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^pm/', include('OET.pm.urls')),
)

from OET.events.urls import urlpatterns as events_urlpatterns
urlpatterns += events_urlpatterns