from django.db import models
from django.contrib.auth.models import User

class Message(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    text = models.CharField(max_length=500)
    from_user = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_from_user")
    to_user = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_to_user")
    unread = models.BooleanField()
    datetime = models.DateTimeField()

    class Meta:
        ordering = ('-datetime',)