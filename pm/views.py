# -*- coding: utf8 -*-

from django.forms import ModelForm
from models import Message
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.template import RequestContext
from django.http import Http404
import datetime
from django.contrib.auth.models import User

class SendForm(ModelForm):
    class Meta:
        model = Message
        exclude = ('to_user', 'from_user', 'datetime', 'unread')

@login_required
def send(request, uid=None):
    template = "pm/send.html"
    template_ok = "pm/sent.html"

    if request.method != 'POST':
        if uid is not None:
            try:
                user = User.objects.get(id=uid)
            except User.DoesNotExists:
                raise Http404
            form = SendForm(initial={"to_user":user})
        else:
            raise Http404
        return render_to_response(template,
                            {"form" : form,
                             "uid" : uid},
                            context_instance=RequestContext(request))
    
    if uid is not None:
        try:
            user = User.objects.get(id=uid)
        except User.DoesNotExists:
            raise Http404
    else:
        raise Http404
    form = SendForm(request.POST)
    #form.additional_data = {"place_str" : request.POST["id_place_str"]}
    place = None
    if form.is_valid():
        obj = form.save(commit=False)
        obj.from_user = request.user
        obj.to_user = user
        obj.datetime = datetime.datetime.today()
        obj.unread = True
        obj = form.save(commit=True)
    else:
        return render_to_response(template,
                            {"form" : form},
                            context_instance=RequestContext(request))
    return render_to_response(template_ok,
                            {'next' : '/pm/view/%s' % (obj.id)},
                            context_instance=RequestContext(request))

@login_required
def view(request, num):
    template = "pm/view.html"
    msg = Message.objects.get(id=num)
    if not((msg.from_user == request.user) or (msg.to_user == request.user)):
        return render_to_response("pm/denied.html",
                            context_instance=RequestContext(request))
    if request.user == msg.to_user:
        msg.unread = False
        msg.save()
    return render_to_response(template,
                            {"msg" : msg},
                            context_instance=RequestContext(request))

@login_required
def pm_inbox(request, page):
    template = "pm/list.html"
    on_page = 5
    s = ((int(page)-1)*on_page)
    msgs = Message.objects.filter(to_user=request.user)[s:s+on_page]

    return render_to_response(template,
                            {"msgs" : msgs, "name" : "Inbox"},
                            context_instance=RequestContext(request))

@login_required
def pm_outbox(request, page):
    template = "pm/list.html"
    on_page = 5
    s = ((int(page)-1)*on_page)
    msgs = Message.objects.filter(from_user=request.user)[s:s+on_page]

    return render_to_response(template,
                            {"msgs" : msgs, "name" : "Outbox"},
                            context_instance=RequestContext(request))