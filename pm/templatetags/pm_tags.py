from django import template
from pm.models import Message
#import pm.models

register = template.Library()

class PMUnreadNode(template.Node):

    def __init__(self, user_id):
        if type(user_id) is int:
            self.user_id = user_id
        else:
            self.user_id = template.Variable(user_id)

    def render(self, context):
        if type(self.user_id) is int:
            usr = self.user_id
        else:
            usr = self.user_id.resolve(context)
        return Message.objects.filter(to_user=int(usr)).filter(unread=True).count()

def do_pm_count(parser, token):
    tag,num = token.split_contents()
    try:
        user = int(num)
    except ValueError:
        user = str(num)
    return PMUnreadNode(user)

register.tag('pm_count', do_pm_count)