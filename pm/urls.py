from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to, direct_to_template

urlpatterns = patterns('pm.views',
    url(r'^$', redirect_to, {'url': '/pm/inbox/', 'permanent': False}, name="pm"),
    #url(r'^send/$', 'send', name='pm_send'),
    url(r'^send/to/(?P<uid>\d+)$', 'send', name='pm_send'),
    url(r'^view/(?P<num>\d+)$', 'view', name='pm_view'),

    url(r'^inbox/$', 'pm_inbox', {"page" : 1}, name='pm_inbox'),
    url(r'^inbox/(?P<page>\d+)$', 'pm_inbox', name='pm_inbox'),
    url(r'^outbox/$', 'pm_outbox', {"page" : 1}, name='pm_outbox'),
    url(r'^outbox/(?P<page>\d+)$', 'pm_outbox', name='pm_outbox'),
)