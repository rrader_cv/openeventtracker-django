import os
import sys

sys.path.append('/srv/')
sys.path.append('/srv/OET/')

os.environ['PYTHON_EGG_CACHE'] = '/srv/.python-egg'
os.environ['DJANGO_SETTINGS_MODULE'] = 'OET.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
