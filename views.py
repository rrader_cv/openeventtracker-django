from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.http import Http404
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

def registration(request):
    if request.method != 'POST':
        return render_to_response('auth/registration.html',
            {"form" : UserCreationForm}, context_instance=RequestContext(request))
    
    form = UserCreationForm(request.POST)
    if not form.is_valid():
       return render_to_response('auth/registration.html',
                    {"form" : UserCreationForm, "error" : 1}, context_instance=RequestContext(request))
    form.save()
    username = form.cleaned_data['username']
    password = form.cleaned_data['password1']
    user = authenticate(username=username, password=password)
    if (user is not None) and (user.is_active):
        login(request, user)
        return render_to_response('auth/registration_success.html', context_instance=RequestContext(request))
    else:
        return render_to_response('auth/registration_success.html', {"nologin":1},
                                  context_instance=RequestContext(request))


def logout_user(request):
    next = None
    if request.GET.has_key('next'):
        next = request.GET['next']
    logout(request)
    return render_to_response('auth/logout_success.html',
                            {"next" : next},
                            context_instance=RequestContext(request))

def login_user(request):
    if request.method != 'POST':
        next = None
        if request.GET.has_key('next'):
            next = request.GET['next']
        return render_to_response('auth/login.html',
          {"form" : AuthenticationForm, "next" : next},
          context_instance=RequestContext(request))
    
    username = request.POST['username']
    password = request.POST['password']
    next = None
    if request.POST.has_key('next'):
        next = request.POST['next']
    user = authenticate(username=username, password=password)

    next_error = "/users/login/"
    if next is not None:
        next_error += "?next=%s" % next

    if user is not None:
        if user.is_active:
            login(request, user)
            return render_to_response('auth/login_success.html',
                                    {"next" : next},
                                    context_instance=RequestContext(request))
        else:
            return render_to_response('auth/login_disabled.html',
                                    {"next" : next_error},
                                    context_instance=RequestContext(request))
    else:
        return render_to_response('auth/login_not_exists.html',
                                    {"next" : next_error},
                                    context_instance=RequestContext(request))

def user_profile(request, user):
    try:
        usr = User.objects.get(id=user)
    except User.DoesNotExists:
        raise Http404
    return render_to_response('auth/user.html',
                            {'usr' : usr},
                            context_instance=RequestContext(request))

@login_required
def my_profile(request):
    return render_to_response('auth/user.html',
                            {'usr' : request.user},
                            context_instance=RequestContext(request))